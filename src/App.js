import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Client from "./component/Client";
import Login from "./component/Login";

import "./styles.css";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login/>} />
        <Route exact path="/client/:id" element={<Client/>} />
      </Routes>
    </BrowserRouter>
    </div>
  );
}
export default App;