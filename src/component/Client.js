import React from 'react';
import { useParams } from "react-router-dom";
import User from "./User";


function Client(){
    const { id } = useParams();
    return (
        <div>
          <User idclient={id}></User>
        </div>
      );
}
export default Client;