import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { get } from "../requests/axiosRequest";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      informacion: [],
      valid: false
    };
  }

  componentDidMount() {
    var id = this.props.idclient
    get("/client/" + id)
      .then((data) => {
        console.log(data);
        this.setState({ informacion: data, valid:true });
      })
      .catch((err) => {
        console.log(err);
        this.setState({ valid:false });
      });
  }

  render() {
    const renderInfo = (
      <div>
        <Typography variant="h3" component="h3">
          Informacion del Cliente
        </Typography>
        <Card sx={{ minWidth: 275 }}>
          <CardContent>
            <Typography variant="h5" component="div">
              {this.state.informacion.nombre} {this.state.informacion.apellido}
            </Typography>
            <Typography
              sx={{ fontSize: 14 }}
              color="text.secondary"
              gutterBottom
            >
              {this.state.informacion.oficio}
            </Typography>
            <Typography sx={{ mb: 1.1 }} color="text.secondary">
              Telefono: {this.state.informacion.telefono}
            </Typography>
            <Typography variant="body2">
              Direccion: {this.state.informacion.direccion}
              <br />
              Ciudad: {this.state.informacion.ciudad}
              <br />
              Email: {this.state.informacion.email}
            </Typography>
          </CardContent>
        </Card>
      </div>
    );

    return (
      <div>
        {this.state.valid ? renderInfo : <div>
        <Typography variant="h3" component="h3">
          Cliente no encontrado
        </Typography></div>}
      </div>
    );
  }
}
export default User;
