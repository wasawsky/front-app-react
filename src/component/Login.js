import { toHaveStyle } from "@testing-library/jest-dom/dist/matchers";
import React, { useState ,useEffect} from "react";
import ReactDOM from "react-dom";
import { Route, Router } from "react-router-dom";
import { Navigate } from "react-router";
import { jwtRequest, loginpost, post } from "../requests/axiosRequest";
import "./styles.css";

function Login() {
    const [errorMessages, setErrorMessages] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [id, setId] = useState(null);
  
    const handleSubmit = (event) => {
      event.preventDefault();
  
      var { uname, pass } = document.forms[0];
  
      var data = {
              "username": uname.value,
              "password": pass.value
          }
      loginpost('user',data).then(res => {
        setId(pass.value)
        setIsSubmitted(true);
      }).catch(err => {
        console.log(err)
        setErrorMessages({ name: "uname", message: "Usuario no encontrado" })
      })
      
    };
  
    const renderErrorMessage = (name) =>
      name === errorMessages.name && (
        <div className="error">{errorMessages.message}</div>
      );
  
    const renderForm = (
      <div className="form">
        <div className="login-form">
          <div className="title">Sign In</div>
          <form onSubmit={handleSubmit}>
            <div className="input-container">
              <label>Username </label>
              <input type="text" name="uname" required />
              {renderErrorMessage("uname")}
            </div>
            <div className="input-container">
              <label>Password </label>
              <input type="password" name="pass" required />
              {renderErrorMessage("pass")}
            </div>
            <div className="button-container">
              <input type="submit" />
            </div>
          </form>
        </div>
      </div>
    );
  
    return (
        
      <div className="login">
          {isSubmitted ? <Navigate to={"/client/"+ id}></Navigate> : renderForm}
      </div>
    );
  }
  
  export default Login;