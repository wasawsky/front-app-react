import axios from 'axios';

const URL = 'https://scotiaback.herokuapp.com/';
var token = window.sessionStorage.getItem("token");
var instance = axios.create({
    baseURL: URL,
    headers: { 'Authorization': 'Bearer ' + token,
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Headers': 'POST, GET, PUT, DELETE, OPTIONS, HEAD, Authorization, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-Allow-Origin'}
});


export const get = (path, params) => {
    return new Promise((resolve, reject) => {
        instance.get(path, params)
            .then(res => {
                resolve(res.data);
            }).catch(err => {
                reject(err)
            });
    });
}

export const post = (path, data, params) => {
    return new Promise((resolve, reject) => {
        instance.post(path, data, params)
            .then(res => {
                resolve(res.data);
            }).catch(err => {
                reject(err)
            });
    });
}

export const put = (path, data, params) => {
    return new Promise((resolve, reject) => {
        console.log(data);
        instance.put(path, data, params)
            .then(res => {
                resolve(res.data);
            }).catch(err => {
                reject(err)
            });
    });
}

export const deleteRequest = (path, data, params) => {
    return new Promise((resolve, reject) => {
        instance.delete(path, data, params)
            .then(res => {
                resolve(res.data);
            }).catch(err => {
                reject(err)
            });
    });
}

export const loginpost = (path, data, params) => {
    return new Promise((resolve, reject) => {
        var instance = axios.create({
            baseURL: URL,
            headers: { 'Access-Control-Allow-Origin':'*',
                        'Access-Control-Allow-Headers': 'POST, GET, PUT, DELETE, OPTIONS, HEAD, Authorization, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-Allow-Origin'}
        });
        instance.post(path, data, params)
            .then(res => {
                window.sessionStorage.setItem("token", res.data);
                instance = axios.create({
                    baseURL: URL,
                    headers: { 'Authorization': 'Bearer ' + res.data}
                });
                resolve(res.data);
            }).catch(err => {
                reject(err)
            });
    });
}